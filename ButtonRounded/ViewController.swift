//
//  ViewController.swift
//  ButtonRounded
//
//  Created by Edy Cu Tjong on 7/1/19.
//  Copyright © 2019 Edy Cu Tjong. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        button.backgroundColor = UIColor.init(red: 40/255, green: 173/255, blue: 99/255, alpha: 1.0)
        button.layer.cornerRadius = 25.0
        button.tintColor = .white
    }


}

